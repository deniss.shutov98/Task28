import templateInnerHtml from "./cart.html";
import { getProductById } from "../../common/scripts/fetch.js";
import CartItem from "../../common/components/cartItem/cartItem.js";
import {
  getStoredProductsIds,
  getProductsAmount,
} from "../../common/scripts/localStorageController.js";

customElements.define("cart-item", CartItem);

const template = document.createElement("template");
template.innerHTML = templateInnerHtml;

export default class Cart extends HTMLElement {
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(template.content.cloneNode(true));

    this.container = this.shadowRoot.getElementById("products-list");
    this.totalValue = 0;
    this.init();
    this.updateCartItems();
  }

  async updateCartItems() {
    this.container.innerHTML = "";
    this.totalValue = 0;
    let productIds = getStoredProductsIds();

    for (let productId of productIds) {
      let product = await getProductById(productId);
      let item = document.createElement("cart-item");
      item.classList.add("cart-item");
      item.product = product;
      item.cart = this;
      item.productsAmount = getProductsAmount(productId);
      this.totalValue += product.price * item.productsAmount;
      this.container.appendChild(item);
    }
    this.setTotalValue();
  }

  init() {
    let closeButton = this.shadowRoot.getElementById("close-button");
    closeButton.addEventListener("click", () => {
      this.shadowRoot.host.style.display = "none";
    });
  }

  setTotalValue() {
    this.shadowRoot.getElementById("total-value").innerHTML = this.totalValue;
  }
}
