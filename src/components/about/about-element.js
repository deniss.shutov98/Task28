import templateInnerHtml from "./about.html";

const template = document.createElement("template");
template.innerHTML = templateInnerHtml;

export default class About extends HTMLElement {
  constructor() {
    super();

    const sr = this.attachShadow({ mode: "open" });
    sr.appendChild(template.content.cloneNode(true));
  }
}
