import SellingItem from "../../common/components/sellingItem/sellingItem.js";
import { getProductById } from "../../common/scripts/fetch.js";
import templateInnerHtml from "./home.html";

customElements.define("selling-item", SellingItem);

const template = document.createElement("template");
template.innerHTML = templateInnerHtml;

export default class Home extends HTMLElement {
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(template.content.cloneNode(true));

    this.cart = {};
  }

  async connectedCallback() {
    const productsContainer =
      this.shadowRoot.getElementById("products-container");

    for (let i = 1; i <= 3; i++) {
      let product = await getProductById(i);
      let item = document.createElement("selling-item");
      item.product = product;
      item.cart = this.cart;
      productsContainer.appendChild(item);
    }

    const buttons = this.shadowRoot.querySelectorAll("button");
    for (let button of buttons) {
      button.addEventListener("click", () =>
        this.navigateToProducts("products-element")
      );
    }
  }
}
