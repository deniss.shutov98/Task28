import templateInnerHtml from "./products.html";
import {
  getAllProducts,
  getProductsCategories,
  getProductsByCategory,
} from "../../common/scripts/fetch.js";

const template = document.createElement("template");
template.innerHTML = templateInnerHtml;

export default class Products extends HTMLElement {
  constructor() {
    super();

    this.cart = {};
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(template.content.cloneNode(true));

    this.initSlider();
    this.initButtons();
  }

  initSlider() {
    let slider = this.shadowRoot.getElementById("range-input");
    let output = this.shadowRoot.getElementById("range-output");

    output.innerHTML = slider.value;
    slider.oninput = () => (output.innerHTML = slider.value);
  }

  initButtons() {
    let filterButton = this.shadowRoot.getElementById("apply-filters-button");
    filterButton.addEventListener("click", () => this.applyFilters());
  }

  async connectedCallback() {
    let products = await getAllProducts();
    this.loadProducts(products.products);
    this.initCategorySelect();
  }

  loadProducts(products) {
    let productsContainer =
      this.shadowRoot.getElementById("products-container");
    productsContainer.innerHTML = "";

    for (let product of products) {
      let item = document.createElement("selling-item");
      item.product = product;
      item.cart = this.cart;
      productsContainer.appendChild(item);
    }
  }

  async initCategorySelect() {
    let categories = await getProductsCategories();
    let categorySelect = this.shadowRoot.getElementById("category-select");

    for (let category of categories) {
      let option = document.createElement("option");
      option.value = category;
      option.text = category;
      categorySelect.appendChild(option);
    }
  }

  async applyFilters() {
    let searchString = this.shadowRoot.getElementById("search").value;
    let category = this.shadowRoot.getElementById("category-select").value;
    let maxPrice = this.shadowRoot.getElementById("range-input").value;
    let products =
      category == "All"
        ? await getAllProducts()
        : await getProductsByCategory(category);

    let results = products.products
      .filter((x) => x.price <= maxPrice)
      .filter((x) =>
        x.title.toLowerCase().includes(searchString.toLowerCase())
      );

    this.loadProducts(results);
  }
}
