export async function getAllProducts() {
  return await fetch("https://dummyjson.com/products")
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
}

export async function getProductById(id) {
  return await fetch(`https://dummyjson.com/products/${id}`)
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
}

export async function getProductsCategories() {
  return await fetch("https://dummyjson.com/products/categories")
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
}

export async function getProductsByCategory(category) {
  return await fetch(`https://dummyjson.com/products/category/${category}`)
    .then((response) => response.json())
    .then((data) => {
      return data;
    });
}
