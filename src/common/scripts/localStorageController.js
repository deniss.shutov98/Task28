export function getStoredProductsIds() {
  return getProductsKeys();
}

export function getProductsAmount(productId) {
  return localStorage.getItem(productId);
}

export function addProductToCart(productId, cart) {
  addProductKey(productId);
  let newItemsAmount = increaseItemsAmount(productId);
  cart.updateCartItems();
  return newItemsAmount;
}

export function removeProductFromCart(productKey, cart) {
  removeProductKey(productKey);
  localStorage.removeItem(productKey);
  cart.updateCartItems();
}

export function decreaseProductsAmountInCart(productId, cart) {
  let newItemsAmount = decreaseItemsAmount(productId);
  cart.updateCartItems();
  return newItemsAmount;
}

function increaseItemsAmount(productKey) {
  let currentAmount = localStorage.getItem(`${productKey}`);
  if (currentAmount == null) {
    currentAmount = 0;
  }
  currentAmount++;
  localStorage.setItem(productKey, currentAmount);
  return currentAmount;
}

function decreaseItemsAmount(productKey) {
  let currentAmount = localStorage.getItem(productKey);
  if (currentAmount - 1 == 0) {
    removeProductKey(productKey);
    localStorage.removeItem(productKey);
    return;
  }
  currentAmount--;
  localStorage.setItem(productKey, currentAmount);
  return currentAmount;
}

function addProductKey(key) {
  let keys = getProductsKeys();
  if (keys.includes(`${key}`)) {
    return;
  }
  keys.push(key);
  updateProductsKeys(keys);
}

function removeProductKey(key) {
  let keys = getProductsKeys();
  let index = keys.indexOf(`${key}`);

  if (index > -1) {
    keys.splice(index, 1);
  }
  updateProductsKeys(keys);
}

function getProductsKeys() {
  let keysString = localStorage.getItem("keys");
  if (keysString == null) {
    return [];
  }
  return keysString.split(",");
}

function updateProductsKeys(keysArray) {
  if (keysArray.length == 0) {
    localStorage.removeItem("keys");
    return;
  }
  let keysString = keysArray.join();
  localStorage.setItem("keys", keysString);
}
