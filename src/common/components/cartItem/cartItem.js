import templateInnerHtml from "./cartItem.html";
import {
  removeProductFromCart,
  decreaseProductsAmountInCart,
  addProductToCart,
} from "../../scripts/localStorageController.js";

const template = document.createElement("template");
template.innerHTML = templateInnerHtml;

export default class CartItem extends HTMLElement {
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(template.content.cloneNode(true));

    this.product = {};
    this.productsAmount = 0;
    this.cart = {};
  }

  connectedCallback() {
    let image = this.shadowRoot.getElementById("image");
    let description = this.shadowRoot.getElementById("description");

    image.src = this.product.thumbnail;
    description.innerHTML = `<b>${this.product.title}</b><br/>
       $${this.product.price}`;

    this.initButtons();
  }

  initButtons() {
    let incrementButton = this.shadowRoot.getElementById("increment-button");
    let decrementButton = this.shadowRoot.getElementById("decrement-button");
    let removeButton = this.shadowRoot.getElementById("remove-button");
    let dataElement = this.shadowRoot.getElementById("counting");
    dataElement.innerHTML = this.productsAmount;

    incrementButton.addEventListener("click", () => {
      this.amount = addProductToCart(this.product.id, this.cart);
      dataElement.innerHTML = this.amount;
    });

    decrementButton.addEventListener("click", () => {
      if (this.amount - 1 == 0) {
        removeProductFromCart(this.product.id, this.cart);
        return;
      }
      this.amount = decreaseProductsAmountInCart(this.product.id, this.cart);
      dataElement.innerHTML = this.amount;
    });

    removeButton.addEventListener("click", () =>
      removeProductFromCart(this.product.id, this.cart)
    );
  }
}
