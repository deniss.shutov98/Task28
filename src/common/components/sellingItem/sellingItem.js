import templateInnerHtml from "./sellingItem.html";
import { addProductToCart } from "../../scripts/localStorageController.js";

const template = document.createElement("template");
template.innerHTML = templateInnerHtml;

export default class SellingItem extends HTMLElement {
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.appendChild(template.content.cloneNode(true));

    this.product = {};
    this.cart = {};

    let item = this.shadowRoot.getElementById("selling-item");
    item.addEventListener("click", () => {
      addProductToCart(this.product.id, this.cart);
    });
  }

  connectedCallback() {
    let image = this.shadowRoot.getElementById("image");
    let description = this.shadowRoot.getElementById("description");

    image.src = this.product.thumbnail;
    description.innerHTML = `<b>${this.product.title}</b><br/>
       $${this.product.price}`;
  }
}
