import "./index.css";

import Home from "./components/home/home-element.js";
import Products from "./components/products/products-element.js";
import Cart from "./components/cart/cart-element.js";
import About from "./components/about/about-element.js";

customElements.define("home-element", Home);
customElements.define("products-element", Products);
customElements.define("cart-element", Cart);
customElements.define("about-element", About);

const body = document.getElementById("body");
const main = document.getElementById("main");
const home = document.getElementById("home");
const products = document.getElementById("products");
const about = document.getElementById("about");
const cartIcon = document.getElementById("cart");
const cartElement = document.createElement("cart-element");

home.addEventListener("click", () => changeContent("home-element"));
products.addEventListener("click", () => changeContent("products-element"));
about.addEventListener("click", () => changeContent("about-element"));
cartIcon.addEventListener("click", () => showCart());

init();

function init() {
  cartElement.classList.add("cart-element");
  body.appendChild(cartElement);
  changeContent("home-element");
}

function changeContent(elementName) {
  let linkItem = document.createElement(elementName);
  linkItem.classList.add(elementName);

  main.innerHTML = "";
  main.appendChild(linkItem);

  if (elementName == "home-element") {
    linkItem.navigateToProducts = changeContent;
    linkItem.cart = cartElement;
  }
  if (elementName == "products-element") {
    linkItem.cart = cartElement;
  }
}

function showCart() {
  cartElement.style.display = "flex";
}
